﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Abstractions
{
    public interface IRectangle : IShape
    {
        decimal Width { get; }
        decimal Height { get; }
    }
}
