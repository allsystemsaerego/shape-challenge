﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Abstractions
{
    public interface ICircle : IShape
    {
         decimal Radius { get; }
    }
}
