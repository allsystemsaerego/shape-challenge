﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Abstractions
{
    public interface ITriangle : IShape
    {
        decimal LengthA { get; }
        decimal LengthB { get; }
        decimal LengthC { get; }
        decimal Height { get; }
    }
}
