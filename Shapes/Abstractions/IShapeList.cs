﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Abstractions
{
    public interface IShapeList
    {
        void AddShape(IShape shape);
        int TotalShapes { get; }
        decimal SumArea();
        decimal SumCorners();
    }
}
