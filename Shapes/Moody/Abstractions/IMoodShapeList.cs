﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Moody
{
    public interface IMoodShapeList :  IShapeList
    {
        void AddShape(IShape shape, Mood mood);
    }
}
