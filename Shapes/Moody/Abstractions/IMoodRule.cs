﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Moody
{
    public interface IMoodRule
    {
        OperationType OperationType { get; }
        bool UseRule(IShape shape);
        decimal GetValue(IShape shape, Mood mood);
    }
}
