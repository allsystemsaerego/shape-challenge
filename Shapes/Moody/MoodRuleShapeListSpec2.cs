﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes.Moody
{
    public class MoodRuleShapeListSpec2 : MoodRuleShapeList
    {
        public MoodRuleShapeListSpec2()
        {
            _moodRules.Add(new Rules.Spec1.SumAreaMultiplicationRule());
            _moodRules.Add(new Rules.Spec2.SumCornersCircleMultiplicationRule());
            _moodRules.Add(new Rules.Spec2.SumCornersNotCircleMultiplicationRule());
        }
    }
}
