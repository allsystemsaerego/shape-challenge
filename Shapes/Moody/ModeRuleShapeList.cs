﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes.Moody
{
    public abstract class MoodRuleShapeList : IShapeList, IMoodShapeList
    {
        protected readonly List<IMoodRule> _moodRules;
        protected readonly Dictionary<IShape, Mood> _innerShapes;

        public MoodRuleShapeList()
        {
            _innerShapes = new Dictionary<IShape, Mood>();
            _moodRules = new List<IMoodRule>();
        }


        public int TotalShapes { get { return _innerShapes.Count; } }

        public virtual void AddShape(IShape shape, Mood mood = Mood.Normal)
        {
            _innerShapes.Add(shape, mood);
        }

        public virtual void AddShape(IShape shape)
        {
            this.AddShape(shape, Mood.Normal);
        }

        public virtual decimal SumArea()
        {
            return _innerShapes
                .Sum(SumAreaForShape);
        }

        public virtual decimal SumCorners()
        {
            return _innerShapes
                .Sum(SumCornersForShape);
        }

        private decimal SumCornersForShape(KeyValuePair<IShape, Mood> shapeMoodKeyValuePair)
        {
            decimal area;

            var shape = shapeMoodKeyValuePair.Key;
            var mood = shapeMoodKeyValuePair.Value;

            var rule = TryGetRule(shapeMoodKeyValuePair.Key, OperationType.SumCorners);

            if (rule != null)
            {
                area = rule.GetValue(shape, mood);
            }
            else
            {
                area = shape.Corners;
            }

            return area;
        }


        private decimal SumAreaForShape(KeyValuePair<IShape, Mood> shapeMoodKeyValuePair)
        {
            decimal area;

            var shape = shapeMoodKeyValuePair.Key;
            var mood = shapeMoodKeyValuePair.Value;

            var rule = TryGetRule(shape, OperationType.SumArea);

            if (rule != null)
            {
                area = rule.GetValue(shape, mood);
            }
            else
            {
                area = shape.GetArea();
            }

            return area;
        }


        private IMoodRule TryGetRule(IShape shape, OperationType operationType)
        {
            // Method probably belongs in a rulecollection class

            return _moodRules
                .FirstOrDefault(r =>
                    r.OperationType == operationType
                    && r.UseRule(shape));
        }

    }
}
