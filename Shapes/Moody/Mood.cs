﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Moody
{
    public enum Mood
    {
        Normal = 1,
        Happy = 2,
        SuperHappy = 3
    }
}
