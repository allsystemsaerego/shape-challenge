﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes.Moody
{
    public class MoodRuleShapeListSpec1 : MoodRuleShapeList
    {
        public MoodRuleShapeListSpec1()
        {
            _moodRules.Add(new Rules.Spec1.SumAreaMultiplicationRule());
            _moodRules.Add(new Rules.Spec1.SumCornersMultiplicationRule());
        }
    }
}
