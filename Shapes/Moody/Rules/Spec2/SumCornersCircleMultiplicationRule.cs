﻿using System;
using System.Collections.Generic;
using System.Text;
using Shapes.Abstractions;

namespace Shapes.Moody.Rules.Spec2
{
    class SumCornersCircleMultiplicationRule : IMoodRule
    {
        public OperationType OperationType { get; } = OperationType.SumCorners;

        public decimal GetValue(IShape shape, Mood mood)
        {
            decimal corners = shape.Corners;

            switch (mood)
            {
                case Mood.SuperHappy:
                    corners = corners * 10;
                    break;
                case Mood.Happy:
                    corners = corners * 5;
                    break;
            }

            return corners;
        }

        public bool UseRule(IShape shape)
        {
            return shape as ICircle != null;
        }
    }
}
