﻿using System;
using System.Collections.Generic;
using System.Text;
using Shapes.Abstractions;

namespace Shapes.Moody.Rules.Spec1
{
    class SumAreaMultiplicationRule : IMoodRule
    {
        public OperationType OperationType { get; } = OperationType.SumArea;

        public decimal GetValue(IShape shape, Mood mood)
        {
            decimal area = shape.GetArea();

            switch (mood)
            {
                case Mood.SuperHappy:
                    area = area * 3;
                    break;
                case Mood.Happy:
                    area = area * 2;
                    break;
            }

            return area;
        }

        public bool UseRule(IShape shape)
        {
            return true;
        }
    }
}
