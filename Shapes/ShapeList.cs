﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes
{
    public class ShapeList : IShapeList
    {
        protected readonly List<IShape> _innerShapes;

        public ShapeList()
        {
            _innerShapes = new List<IShape>();
        }

        public int TotalShapes { get { return _innerShapes.Count; } }

        public virtual void AddShape(IShape shape) 
        {
            if(shape == null)
            {
                throw new ArgumentNullException(nameof(shape));
            }

            _innerShapes.Add(shape);
        }

        public virtual decimal SumArea()
        {
            return _innerShapes
                .Sum(x => x.GetArea());
        }

        public virtual decimal SumCorners()
        {
            return _innerShapes
                .Sum(x => x.Corners);
        }
    }
}
