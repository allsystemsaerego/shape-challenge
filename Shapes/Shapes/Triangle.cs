﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Shapes
{
    public class Triangle : ITriangle
    {
        public decimal LengthA { get; }
        public decimal LengthB { get; }
        public decimal LengthC { get; }
        public decimal Height { get; }
        public int Corners { get; } = 3;

        public Triangle(decimal lengthA, decimal lengthB, decimal lengthC, decimal height)
        {
            if (lengthA <= 0)
            {
                throw new IndexOutOfRangeException(nameof(lengthA));
            }

            if (lengthB <= 0)
            {
                throw new IndexOutOfRangeException(nameof(lengthB));
            }

            if (lengthC <= 0)
            {
                throw new IndexOutOfRangeException(nameof(lengthC));
            }

            if (height <= 0)
            {
                throw new IndexOutOfRangeException(nameof(height));
            }

            LengthA = lengthA;
            LengthB = lengthB;
            LengthC = lengthC;
            Height = height;
        }

        public decimal GetArea()
        {
            return (LengthB * Height) / 2;
        }
    }
}
