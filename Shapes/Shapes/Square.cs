﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Shapes
{
    public class Square : Rectangle, ISquare
    {
        public Square(decimal length) : base(length, length)
        {

        }
    }
}
