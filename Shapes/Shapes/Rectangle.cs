﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Shapes
{
    public class Rectangle : IRectangle
    {
        public decimal Width { get; }
        public decimal Height { get; }
        public int Corners { get; } = 4;

        public Rectangle(decimal width, decimal height)
        {
            if (width <= 0)
            {
                throw new IndexOutOfRangeException(nameof(width));
            }

            if (height <= 0)
            {
                throw new IndexOutOfRangeException(nameof(height));
            }

            Width = width;
            Height = height;
        }

        public decimal GetArea()
        {
            return Width * Height;
        }
    }
}
