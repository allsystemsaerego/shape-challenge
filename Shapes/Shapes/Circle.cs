﻿using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Shapes
{
    public class Circle : ICircle
    {
        public decimal Radius { get; }

        public int Corners { get; } = 0;

        public Circle(decimal radius)
        {
            if (radius <= 0)
            {
                throw new IndexOutOfRangeException(nameof(radius));
            }

            Radius = radius;
        }

        public decimal GetArea()
        {
            return (decimal)Math.PI * (Radius * Radius);
        }
    }
}
