﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shapes.Abstractions;
using Shapes.Shapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Test.Shapes
{
    [TestClass]
    public class CircleTests
    {
        [TestMethod]
        public void WhenInitialisedRadiusSet()
        {
            int radius = 6;

            ICircle circle = new Circle(radius);

            Assert.AreEqual(radius, circle.Radius);
        }


        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void WhenInitialisedWithNotPositiveLengthIndexOutOfRangeExceptionThrown()
        {
            int radius = new Random().Next(int.MinValue, 0);

            ICircle circle = new Circle(radius);
        }

        [TestMethod]
        public void WhenGetAreaInvokedCorrectAreaIsReturned()
        {
            int radius = 6;

            ICircle circle = new Circle(radius);

            var actualArea = Math.Round(circle.GetArea(), 1);

            var expectedArea = (decimal)113.1;

            Assert.AreEqual(actualArea, expectedArea);
        }
    }
}
