﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shapes.Abstractions;
using Shapes.Shapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Test.Shapes
{
    [TestClass]
    public class RectangleTest
    {
        [TestMethod]
        public void WhenInitialisedWidthPropertySet()
        {
            int width = 1;
            int height = 2;

            IRectangle square = new Rectangle(width, height);

            Assert.AreEqual(width, square.Width);
        }

        [TestMethod]
        public void WhenInitialisedHeightPropertySet()
        {
            int width = 1;
            int height = 2;

            IRectangle square = new Rectangle(width, height);

            Assert.AreEqual(height, square.Height);
        }

        [TestMethod]
        public void WhenGetAreaInvokedCorrectAreaIsReturned()
        {
            int width = 1;
            int height = 2;

            IRectangle rectangle = new Rectangle(width, height);

            var expectedArea = 2;

            var actualArea = rectangle.GetArea();
            
            Assert.AreEqual(expectedArea, actualArea);


            int width2 = 4;
            int height2 = 2;

            IRectangle rectangle2 = new Rectangle(width2, height2);

            var expectedArea2 = 8;

            var actualArea2 = rectangle2.GetArea();

            Assert.AreEqual(expectedArea2, actualArea2);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void WhenInitialisedWithNotPositiveWidthIndexOutOfRangeExceptionThrown()
        {
            int width = new Random().Next(int.MinValue, 0);
            int height = 2;

            IRectangle square = new Rectangle(width, height);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void WhenInitialisedWithNotPositiveHeightIndexOutOfRangeExceptionThrown()
        {
            int width = 1;
            int height = new Random().Next(int.MinValue, 0);

            IRectangle square = new Rectangle(width, height);
        }

    }
}
