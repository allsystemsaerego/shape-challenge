﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shapes.Abstractions;
using Shapes.Shapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Test.Shapes
{
    [TestClass]
    public class SquareTests
    {
        [TestMethod]
        public void WhenInitialisedWidthSet()
        {
            int length = 1;

            ISquare square = new Square(length);

            Assert.AreEqual(length, square.Width);
        }

        [TestMethod]
        public void WhenInitialisedHeightSet()
        {
            int length = 2;

            ISquare square = new Square(length);

            Assert.AreEqual(length, square.Height);
        }

        [TestMethod]
        public void WhenGetAreaInvokedCorrectAreaIsReturned()
        {
            int length = 2;

            ISquare square = new Square(length);

            var expectedArea = 4;

            var actualArea = square.GetArea();

            Assert.AreEqual(expectedArea, actualArea);



            int length2 = 4;

            ISquare square2 = new Square(length2);

            var expectedArea2 = 16;

            var actualArea2 = square2.GetArea();

            Assert.AreEqual(expectedArea2, actualArea2);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void WhenInitialisedWithNotPositiveLengthIndexOutOfRangeExceptionThrown()
        {
            int length = new Random().Next(int.MinValue, 0);

            ISquare square = new Square(length);
        }
    }
}
