﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shapes.Abstractions;
using Shapes.Shapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Test.Shapes
{
    [TestClass]
    public class TriangleTests
    {
        [TestMethod]
        public void WhenInitialisedLengthAPropertySet()
        {
            int lengthA = 1;
            int lengthB = 2;
            int lengthC = 3;
            int height = 4;

            ITriangle triangle = new Triangle(lengthA, lengthB, lengthC, height);

            Assert.AreEqual(lengthA, triangle.LengthA);
        }

        [TestMethod]
        public void WhenInitialisedLengthBPropertySet()
        {
            int lengthA = 1;
            int lengthB = 2;
            int lengthC = 3;
            int height = 4;

            ITriangle triangle = new Triangle(lengthA, lengthB, lengthC, height);

            Assert.AreEqual(lengthB, triangle.LengthB);
        }

        [TestMethod]
        public void WhenInitialisedLengthCPropertySet()
        {
            int lengthA = 1;
            int lengthB = 2;
            int lengthC = 3;
            int height = 4;

            ITriangle triangle = new Triangle(lengthA, lengthB, lengthC, height);

            Assert.AreEqual(lengthC, triangle.LengthC);
        }

        [TestMethod]
        public void WhenGetAreaInvokedCorrectAreaIsReturned()
        {
            int lengthA = 1;
            int lengthB = 20;
            int lengthC = 3;
            int height = 10;

            ITriangle triangle = new Triangle(lengthA, lengthB, lengthC, height);

            var expectedArea = 100;

            var actualArea = triangle.GetArea();

            Assert.AreEqual(expectedArea, actualArea);

            int lengthA2 = 5;
            int lengthB2 = 11;
            int lengthC2 = 7;
            int height2 = 30;

            ITriangle triangle2 = new Triangle(lengthA2, lengthB2, lengthC2, height2);

            var expectedArea2 = 165;

            var actualArea2 = triangle2.GetArea();

            Assert.AreEqual(expectedArea2, actualArea2);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void WhenInitialisedWithNotPositiveLengthAIndexOutOfRangeExceptionThrown()
        {
            int lengthA = new Random().Next(int.MinValue, 0);
            int lengthB = 2;
            int lengthC = 3;
            int height = 4;

            ITriangle triangle = new Triangle(lengthA, lengthB, lengthC, height);
        }
        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void WhenInitialisedWithNotPositiveLengthBIndexOutOfRangeExceptionThrown()
        {
            int lengthA = 1;
            int lengthB = new Random().Next(int.MinValue, 0);
            int lengthC = 3;
            int height = 4;

            ITriangle triangle = new Triangle(lengthA, lengthB, lengthC, height);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void WhenInitialisedWithNotPositiveLengthCIndexOutOfRangeExceptionThrown()
        {
            int lengthA = 1;
            int lengthB = 2;
            int lengthC = new Random().Next(int.MinValue, 0);
            int height = 4;

            ITriangle triangle = new Triangle(lengthA, lengthB, lengthC, height);
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void WhenInitialisedWithNotPositiveHeightCIndexOutOfRangeExceptionThrown()
        {
            int lengthA = 1;
            int lengthB = 2;
            int lengthC = 3;
            int height = new Random().Next(int.MinValue, 0);

            ITriangle triangle = new Triangle(lengthA, lengthB, lengthC, height);
        }
    }
}
