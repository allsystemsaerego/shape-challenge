﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shapes.Abstractions;
using Shapes.Moody;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Test
{
    [TestClass]
    public class MoodyShapeListSpec2Tests
    {
        [TestMethod]
        public void WhenInvokingSumAreaTheSumOfAllShapesAreReturnedWithHappyMultiplier()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            int happyMultiplier = 2;

            var mockShape20 = new Mock<IShape>();
            var mockShape10 = new Mock<IShape>();

            mockShape20.Setup(x => x.GetArea()).Returns(20);
            mockShape10.Setup(x => x.GetArea()).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.Happy);

            var totalArea1 = shapeList.SumArea();

            Assert.AreEqual(20 * happyMultiplier, totalArea1);

            shapeList.AddShape(mockShape10.Object, Mood.Happy);

            var totalArea2 = shapeList.SumArea();

            Assert.AreEqual(30 * happyMultiplier, totalArea2);
        }

        [TestMethod]
        public void WhenInvokingSumAreaTheSumOfAllShapesAreReturnedWithSuperHappyMultiplier()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            var mockShape20 = new Mock<IShape>();
            var mockShape10 = new Mock<IShape>();

            int superHappyMultiplier = 3;

            mockShape20.Setup(x => x.GetArea()).Returns(20);
            mockShape10.Setup(x => x.GetArea()).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.SuperHappy);

            var totalArea1 = shapeList.SumArea();

            Assert.AreEqual(20 * superHappyMultiplier, totalArea1);

            shapeList.AddShape(mockShape10.Object, Mood.SuperHappy);

            var totalArea2 = shapeList.SumArea();

            Assert.AreEqual(30 * superHappyMultiplier, totalArea2);
        }

        [TestMethod]
        public void WhenInvokingSumAreaTheSumOfAllShapesAreReturnedWithNormalMultiplier()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            var mockShape20 = new Mock<IShape>();
            var mockShape10 = new Mock<IShape>();

            mockShape20.Setup(x => x.GetArea()).Returns(20);
            mockShape10.Setup(x => x.GetArea()).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.Normal);

            var totalArea1 = shapeList.SumArea();

            Assert.AreEqual(20, totalArea1);

            shapeList.AddShape(mockShape10.Object); // Demonstrates default of normal

            var totalArea2 = shapeList.SumArea();

            Assert.AreEqual(30, totalArea2);
        }


        [TestMethod]
        public void WhenInvokingSumCornersTheSumOfAllShapesCornersAreReturnedWithHappyMultiplier()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            int happyMultiplier = 2;

            var mockShape20 = new Mock<IShape>();
            var mockShape10 = new Mock<IShape>();

            mockShape20.Setup(x => x.Corners).Returns(20);
            mockShape10.Setup(x => x.Corners).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.Happy);

            var totalCorners = shapeList.SumCorners();

            Assert.AreEqual(20 * happyMultiplier, totalCorners);

            shapeList.AddShape(mockShape10.Object, Mood.Happy);

            var totalCorners2 = shapeList.SumCorners();

            Assert.AreEqual(30 * happyMultiplier, totalCorners2);
        }

        [TestMethod]
        public void WhenInvokingSumCornersTheSumOfAllShapesCornersAreReturnedWithSuperHappyMultiplier()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            var mockShape20 = new Mock<IShape>();
            var mockShape10 = new Mock<IShape>();

            int superHappyMultiplier = 3;

            mockShape20.Setup(x => x.Corners).Returns(20);
            mockShape10.Setup(x => x.Corners).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.SuperHappy);

            var totalCorners = shapeList.SumCorners();

            Assert.AreEqual(20 * superHappyMultiplier, totalCorners);

            shapeList.AddShape(mockShape10.Object, Mood.SuperHappy);

            var totalCorners2 = shapeList.SumCorners();

            Assert.AreEqual(30 * superHappyMultiplier, totalCorners2);
        }

        [TestMethod]
        public void WhenInvokingSumCornersTheSumOfAllShapesCornersAreReturnedWithNormalMultiplier()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            var mockShape20 = new Mock<IShape>();
            var mockShape10 = new Mock<IShape>();

            mockShape20.Setup(x => x.Corners).Returns(20);
            mockShape10.Setup(x => x.Corners).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.Normal);

            var totalCorners = shapeList.SumCorners();

            Assert.AreEqual(20, totalCorners);

            shapeList.AddShape(mockShape10.Object, Mood.Normal);

            var totalCorners2 = shapeList.SumCorners();

            Assert.AreEqual(30, totalCorners2);
        }



        [TestMethod]
        public void WhenInvokingSumCornersTheSumOfAllShapesCornersAreReturnedWithHappyMultiplierAndCustomCircleRule()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            int happyMultiplier = 5;

            var mockShape20 = new Mock<ICircle>();
            var mockShape10 = new Mock<ICircle>();

            mockShape20.Setup(x => x.Corners).Returns(20);
            mockShape10.Setup(x => x.Corners).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.Happy);

            var totalCorners = shapeList.SumCorners();

            Assert.AreEqual(20 * happyMultiplier, totalCorners);

            shapeList.AddShape(mockShape10.Object, Mood.Happy);

            var totalCorners2 = shapeList.SumCorners();

            Assert.AreEqual(30 * happyMultiplier, totalCorners2);
        }

        [TestMethod]
        public void WhenInvokingSumCornersTheSumOfAllShapesCornersAreReturnedWithSuperHappyMultiplierAndCustomCircleRule()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            var mockShape20 = new Mock<ICircle>();
            var mockShape10 = new Mock<ICircle>();

            int superHappyMultiplier = 10;

            mockShape20.Setup(x => x.Corners).Returns(20);
            mockShape10.Setup(x => x.Corners).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.SuperHappy);

            var totalCorners = shapeList.SumCorners();

            Assert.AreEqual(20 * superHappyMultiplier, totalCorners);

            shapeList.AddShape(mockShape10.Object, Mood.SuperHappy);

            var totalCorners2 = shapeList.SumCorners();

            Assert.AreEqual(30 * superHappyMultiplier, totalCorners2);
        }

        [TestMethod]
        public void WhenInvokingSumCornersTheSumOfAllShapesCornersAreReturnedWithNormalMultiplierAndCustomCircleRule()
        {
            var shapeList = new MoodRuleShapeListSpec2();

            var mockShape20 = new Mock<ICircle>();
            var mockShape10 = new Mock<ICircle>();

            mockShape20.Setup(x => x.Corners).Returns(20);
            mockShape10.Setup(x => x.Corners).Returns(10);

            shapeList.AddShape(mockShape20.Object, Mood.Normal);

            var totalCorners = shapeList.SumCorners();

            Assert.AreEqual(20, totalCorners);

            shapeList.AddShape(mockShape10.Object, Mood.Normal);

            var totalCorners2 = shapeList.SumCorners();

            Assert.AreEqual(30, totalCorners2);
        }
    }
}
