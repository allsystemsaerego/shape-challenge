﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shapes.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shapes.Test.Shapes
{
    [TestClass]
    public class ShapeListTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void WhenNullShapePassedIArgumentNullExceptionThrown()
        {
            IShapeList shapeList = new ShapeList();
            IShape shape = null;

            shapeList.AddShape(shape);
        }

        [TestMethod]
        public void WhenShapePassedInShapeCountUpdatesAccordingly()
        {
            IShapeList shapeList = new ShapeList();
            IShape shape = new Mock<IShape>().Object;
            IShape shape2 = new Mock<IShape>().Object;

            shapeList.AddShape(shape);

            Assert.AreEqual(1, shapeList.TotalShapes);

            shapeList.AddShape(shape2);

            Assert.AreEqual(2, shapeList.TotalShapes);
        }

        [TestMethod]
        public void WhenInvokingSumAreaTheSumOfAllShapesAreReturned()
        {
            IShapeList shapeList = new ShapeList();

            var mockShape20 = new Mock<IShape>();
            var mockShape10 = new Mock<IShape>();

            mockShape20.Setup(x => x.GetArea()).Returns(20);
            mockShape10.Setup(x => x.GetArea()).Returns(10);

            shapeList.AddShape(mockShape20.Object);

            var totalArea1 = shapeList.SumArea();

            Assert.AreEqual(totalArea1, 20);

            shapeList.AddShape(mockShape10.Object);

            var totalArea2 = shapeList.SumArea();

            Assert.AreEqual(totalArea2, 30);
        }


        [TestMethod]
        public void WhenInvokingSumCornersTheSumOfAllCornersAreReturned()
        {
            IShapeList shapeList = new ShapeList();

            var mockRectangle = new Mock<IShape>();
            var mockTriangle = new Mock<IShape>();

            mockRectangle.Setup(x => x.Corners).Returns(4);
            mockTriangle.Setup(x => x.Corners).Returns(3);

            shapeList.AddShape(mockRectangle.Object);

            var totalCorners = shapeList.SumCorners();

            Assert.AreEqual(4, totalCorners);

            shapeList.AddShape(mockTriangle.Object);

            var totalCorners2 = shapeList.SumCorners();

            Assert.AreEqual(7, totalCorners2);
        }
    }
}
